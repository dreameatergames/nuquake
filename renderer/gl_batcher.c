// gl_batcher.c: handles creating little batches of polys

#include "quakedef.h"

// these could go in a vertexbuffer/indexbuffer pair
#define MAX_BATCHED_SURFVERTEXES 8096
#define MAX_BATCHED_SURFINDEXES 32000 //24288
const int sizeofvertex = VERTEXSIZE * sizeof(float);

static float r_batchedsurfvertexes[MAX_BATCHED_SURFVERTEXES * VERTEXSIZE];
static unsigned short r_batchedsurfindexes[MAX_BATCHED_SURFINDEXES];

static int r_numsurfvertexes = 0;
static int r_numsurfindexes = 0;

static qboolean multi = false;

void R_BeginBatchingSurfaces (int texcoordindex)
{
   glEnableClientState(GL_VERTEX_ARRAY);
   glVertexPointer(3, GL_FLOAT, sizeofvertex, &r_batchedsurfvertexes[0]);

   glEnableClientState(GL_TEXTURE_COORD_ARRAY);
   glTexCoordPointer(2, GL_FLOAT, sizeofvertex, &r_batchedsurfvertexes[texcoordindex]);

   r_numsurfvertexes = 0;
   r_numsurfindexes = 0;
   
   multi=false;
}

void R_BeginBatchingSurfacesMulti ()
{

   glEnableClientState(GL_VERTEX_ARRAY);
   glVertexPointer(3, GL_FLOAT, sizeofvertex, &r_batchedsurfvertexes[0]);

   glClientActiveTextureARB(GL_TEXTURE0);
   glEnableClientState(GL_TEXTURE_COORD_ARRAY);
   glTexCoordPointer(2, GL_FLOAT, sizeofvertex, &r_batchedsurfvertexes[3]);

   glClientActiveTextureARB(GL_TEXTURE1);
   glEnableClientState(GL_TEXTURE_COORD_ARRAY);
   glTexCoordPointer(2, GL_FLOAT, sizeofvertex, &r_batchedsurfvertexes[5]);

   r_numsurfvertexes = 0;
   r_numsurfindexes = 0;

   multi = true;
}

void R_EndBatchingSurfaces (void)
{
   if (r_numsurfvertexes && r_numsurfindexes)
   {
      glDrawElements(GL_TRIANGLES, r_numsurfindexes, GL_UNSIGNED_SHORT, r_batchedsurfindexes);
   }

   r_numsurfvertexes = 0;
   r_numsurfindexes = 0;
   memset(r_batchedsurfindexes, 0x0, MAX_BATCHED_SURFINDEXES);
   memset(r_batchedsurfvertexes, 0x0, MAX_BATCHED_SURFVERTEXES);

   multi = false;
}

void R_BatchSurface(glpoly_t *p)
{
   int i;
   int numindexes = (p->numverts - 2) * 3;
   unsigned short *ndx;

   if (r_numsurfvertexes + p->numverts >= MAX_BATCHED_SURFVERTEXES)
      R_EndBatchingSurfaces();
   if (r_numsurfindexes + numindexes >= MAX_BATCHED_SURFINDEXES)
      R_EndBatchingSurfaces();

   memcpy(&r_batchedsurfvertexes[r_numsurfvertexes * VERTEXSIZE], p->verts, p->numverts * sizeofvertex);
   ndx = &r_batchedsurfindexes[r_numsurfindexes];

   for (i = 2; i < p->numverts; i++, ndx += 3)
   {
      ndx[0] = r_numsurfvertexes;
      ndx[1] = r_numsurfvertexes + i - 1;
      ndx[2] = r_numsurfvertexes + i;
   }
   //printf("%s: total verts %d\n",__func__, i);
   r_numsurfvertexes += p->numverts;
   r_numsurfindexes += numindexes;
}