/*
 * File: gl_batcher.h
 * Project: renderer
 * File Created: Saturday, 23rd March 2019 5:42:06 pm
 * Author: Hayden Kowalchuk (hayden@hkowsoftware.com)
 * -----
 * Copyright (c) 2019 Hayden Kowalchuk
 */

#ifndef __GL_BATCHER__
#define __GL_BATCHER__

#include "gl_model.h"

void R_BeginBatchingSurfaces(int texcoordindex);
void R_BeginBatchingSurfacesMulti();
void R_EndBatchingSurfaces(void);
void R_BatchSurface(glpoly_t *p);

#endif /* __GL_BATCHER__ */